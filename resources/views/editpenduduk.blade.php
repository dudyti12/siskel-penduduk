@extends('layouts.app')

@section('content')
< 
<div class="container">
    <div class="row">
        <div class="card">
            <div class="col-md-12">
                <h1>Ini Halaman Edit Data</h1>  
                <form action="/penduduk/{{$data_warga->iddata_warga}}" method="POST"> 
                    @csrf
                    @method('PUT')
                <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">NIK</span>
                    <input name ="nik" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('nik') ? old('nik') : $data_warga->nik}}">
                  </div>

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Nama</span>
                    <input name ="nama_warga" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('nama_warga') ? old('nama_warga') : $data_warga->nama_warga}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Tanggal Lahir</span>
                    <input name ="tanggal_lahir" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('tanggal_lahir') ? old('tanggal_lahir') : $data_warga->tanggal_lahir}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Jenis Kelamin</span>
                    <input name ="jenis_kelamin" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('jenis_kelamin') ? old('jenis_kelamin') : $data_warga->jenis_kelamin}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Agama</span>
                    <input name ="agama" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('agama') ? old('agama') : $data_warga->agama}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Golongan Darah</span>
                    <input name ="gol_darah" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('gol_darah') ? old('gol_darah') : $data_warga->gol_darah}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Pendidikan</span>
                    <input name ="pendidikan" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('pendidikan') ? old('pendidikan') : $data_warga->pendidikan}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Pekerjaan</span>
                    <input name ="pekerjaan" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('pekerjaan') ? old('pekerjaan') : $data_warga->pekerjaan}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">No Akta Lahir</span>
                    <input name ="no_akta_lahir" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('no_akta_lahir') ? old('no_akta_lahir') : $data_warga->no_akta_lahir}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">No Akta Kawin</span>
                    <input name ="no_akta_kawin" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('no_akta_kawin') ? old('no_akta_kawin') : $data_warga->no_akta_kawin}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Tempat Lahir</span>
                    <input name ="tempat_lahir" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('tempat_lahir') ? old('tempat_lahir') : $data_warga->tempat_lahir}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Status Kawin</span>
                    <input name ="status_kawin" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('status_kawin') ? old('status_kawin') : $data_warga->status_kawin}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Nama Ayah</span>
                    <input name ="nama_ayah" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('nama_ayah') ? old('nama_ayah') : $data_warga->nama_ayah}}">
                  </div> 

                  <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Nama Ibu</span>
                    <input name ="nama_ibu" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                    value="{{old('nama_ibu') ? old('nama_ibu') : $data_warga->nama_ibu}}">
                  </div> 

                 

                  <input class="btn btn-primary" type="submit" value="Submit">

            </div>
        </form>


        </div>


    </div>



</div>
@endsection
@extends('layouts.master')
@section('title','Sistem Kelurahan')
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Surat Keluar</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Surat</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">  
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left"><strong>Buat Surat ++</strong></div>
                    <div class="pull-right">
                        <a href="{{url('surat')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{'/surat/update/'. $wargas->id}}" method="POST">
                            @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama_warga" value="{{old('nama_warga', $wargas->nama_warga)}}" class="form-control @error('nama_warga') is-invalid @enderror"  value="{{old('nama_warga')}}" readonly >
                                    @error('nama_warga')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                              
                                
                                <div class="form-group">
                                    <label>nik</label>
                                    <input type="text" name="nik" value="{{old('nik', $wargas->nik)}}" class="form-control @error('nik') is-invalid @enderror"  value="{{old('nik')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" value="{{old('tempat_lahir', $wargas->tempat_lahir)}}" class="form-control @error('tempat_lahir') is-invalid @enderror"  value="{{old('tempat_lahir')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" name="tanggal_lahir" value="{{old('tanggal_lahir', $wargas->tanggal_lahir)}}" class="form-control @error('tanggal_lahir') is-invalid @enderror"  value="{{old('tanggal_lahir')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Agama</label>
                                    <input type="text" name="id_agama" value="{{old('id_agama', $wargas->agama->nama_agama)}}" class="form-control @error('id_agama') is-invalid @enderror"  value="{{old('nama_agama')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <input type="text" name="id_jenis_kelamin" value="{{old('id_jenis_kelamin', $wargas->kelamin->jenis_kelamin)}}" class="form-control @error('id_jenis_kelamin') is-invalid @enderror"  value="{{old('jenis_kelamin')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Status Nikah</label>
                                    <input type="text" name="id_kawin" value="{{old('id_kawin', $wargas->detail->status_nikah)}}" class="form-control @error('id_kawin') is-invalid @enderror"  value="{{old('status_nikah')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Pekerjaan</label>
                                    <input type="text" name="pekerjaan" value="{{old('pekerjaan', $wargas->pekerjaan)}}" class="form-control @error('pekerjaan') is-invalid @enderror"  value="{{old('pekerjaan')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="id_kepala_keluarga" value="{{old('alamat_tinggal', $wargas->kk->alamat_tinggal)}}" class="form-control @error('alamat_tinggal') is-invalid @enderror"  value="{{old('alamat_tinggal')}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Keperluan</label>
                                    <select type="text" name="id_keperluan"  class="form-control @error('id_keperluan') is-invalid @enderror" required>
                                    <option value="">--Pilih Keperluan</option>
                                    @foreach($surat as $item)
                                        <option value="{{$item->id}}">{{$item->perihal}}</option>
                                    @endforeach
                                    </select>
                                    @error('id_keperluan')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>     
                                <div class="form-group">
                                    <label>Nomor Surat</label>
                                    <select type="text" name="id_keperluan"  class="form-control @error('id_keperluan') is-invalid @enderror" required>
                                    <option value="">--Pilih Nomor Surat</option>
                                    @foreach($surat as $item)
                                        <option value="{{$item->id}}">{{$item->nomor}}</option>
                                    @endforeach
                                    </select>
                                    @error('id_keperluan')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Surat</label>
                                    <input type="date" id="start" name="tanggal_surat" value="{{old('tanggal_surat', $wargas->tanggal_surat)}}" class="form-control @error('tanggal_surat') is-invalid @enderror"  value="{{old('tanggal_surat')}}" required>
                                </div>
                                <div class="form-group">
                                    <label>Perihal</label>
                                    <textarea type="text" name="perihal" value="{{old('perihal')}}"  class="form-control @error('perihal') is-invalid @enderror" value="{{old('perihal')}}" required>
                                    </textarea>
                                </div>   
                                
                               
                              
                                
                              
                              
                             
                            
                              
                                                                       
                                <button type="submit" class="btn btn-success">Buat</button>
                                
                            </form>
                        </div>
                    </div>                   
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   

    <title>Document</title>
    <style>
p.ex1 {
  margin: 15px;
}

#judul{
            text-align:center;
        }

        #halaman{
            width: auto; 
            height: auto; 
            position: absolute; 
            border: 1px solid; 
            padding-top: 30px; 
            padding-left: 30px; 
            padding-right: 30px; 
            padding-bottom: 80px;
        }
#open {

            text-align: left;
            width: auto;
            height: auto;
            position: absolute;
            padding-top: 20px;

}
div.ttd {

text-align: center;
width: auto;
height: auto;
position: absolute; 
padding-left: 28rem; 
}

p.indent{ padding-left: 1.8em }
p.p{ padding-left: 1rem; }
</style>
</head>
<body>
<div>
<img src="{{ asset('img/kop.PNG') }}" >
</div>

<h3><p id="judul"><u><b>SURAT KETERANGAN</b></u><br>
Nomor : {{$warga->surat->nomor}} </p></h3>

<br>
<p class="indent">&emsp;&nbsp;&nbsp;Yang Bertanda tangan di bawah ini Kepala Lingkungan Lembaga kemasyarakatan Lingkungan Karang Sukun Baru Kelurahan Mataram Timur, Kecamatan Mataram, Kota Mataram menerangkan dengan sebenarnya kepada:<p>
 
  <div class="indent">
    <p class="p">&nbsp;&nbsp;&nbsp;Nama                 : {{$warga->nama_warga}}  </p>    
    <p  class="p">&nbsp;&nbsp;&nbsp;NIK                  :  <th>{{$warga->nik}}</th>                                </p> 
    <p  class="p">&nbsp;&nbsp;&nbsp;Tempat/Tanggal Lahir :   <th>{{$warga->tempat_lahir}}</th>,<th>{{Carbon\Carbon::parse($warga->tanggal_lahir)->isoFormat('D MMMM Y')}}</th>                               </p> 
    <p  class="p">&nbsp;&nbsp;&nbsp;Jenis Kelamin        :  <th>{{$warga->kelamin->jenis_kelamin}}</th>                                  </p> 
    <p  class="p">&nbsp;&nbsp;&nbsp;Status Perkawinan    :   <th>{{$warga->detail->status_nikah}}</th>                              </p> 
    <p  class="p">&nbsp;&nbsp;&nbsp;Agama                :   <th>{{$warga->agama->nama_agama}}</th>                              </p> 
    <p  class="p">&nbsp;&nbsp;&nbsp;Pekerjaan            :      <th>{{$warga->pekerjaan}}</th>                             </p> 
    <p  class="p">&nbsp;&nbsp;&nbsp;Alamat               :      <th>{{$warga->Kk->alamat_tinggal}}</th>                            </p> 
    </div>
    <div class="indent">
    <p class="indent">&emsp;&nbsp;&nbsp;Bahwa yang tersebut namanya diatas adalah memang benar penduduk yang berdomisili tinggal di Wilayah kami sebagai warga RT 0 {{$warga->kk->rukun_tetangga}}/RW 0 {{$warga->kk->rukun_warga}} , Lingkungan Karang Sukun Baru, Kelurahan Mataram Timur, Kecamatan Mataram.</p>
    <p class="indent">&emsp;&nbsp;&nbsp;Adapun Surat keterangan ini diberikan kepada yang bersangkutan untuk keperluan <b>{{$warga->perihal}}</b></p>
    <p class="indent">&emsp;&nbsp;&nbsp;Demikian Surat Pengantar ini kami buat dengan sebenarnya untuk dapat digunakan sebagaimana mestinya</p>
    </div>   
    <br>
    
    <div class="ttd">
   <p> Mataram,  {{Carbon\Carbon::now()->isoFormat('D MMMM Y')}}  <br>
  Lembaga Kemasyarakatan Karang Sukun Baru,<br>
  Kepala Lingkungan</p> 
    <br>
    <br>
    <strong><u><p >Desi Indrani Pertiwi</p></u></strong>    
    </div>
        
     
  
    
     

<script type="text/javascript"> 
    window:print(); 

</script>
</body>
     
    


</html>
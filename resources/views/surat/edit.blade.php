@extends('layouts.master')
@section('title','Sistem Kelurahan')
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">  
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left"><strong>Tambah Data Surat Keluar</strong></div>
                    <div class="pull-right">
                        <a href="{{url('print')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{url('print/'.$prints->id)}}" method="POST">
                                @csrf
                                @method('patch')
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" value="{{old('nama', $prints->nama)}}"" class="form-control @error('nama') is-invalid @enderror" value="{{old('nama')}}" required autofocus >
                                 
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Tanggal</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input class="form-control @error('nama') is-invalid @enderror" value="{{old('tanggal_surat', $prints->tanggal_surat)}}" name="tanggal_surat" value="{{old('tanggal_surat')}}" required>
                                    </div>
                                    <small class="form-text text-muted">format. DD/MM/YYYY</small>
                                </div>
                                <div class="row form-group">
                            <div class="col col-md-6">
                            <label for="textarea-input" class=" form-control-label">Perihal:</label></div>
                            <div class="col-12 col-md-12">
                            <textarea name="perihal"  id="textarea-input" rows="9"  value="{{old('perihal')}}" class="form-control" required>{{$prints->perihal}}</textarea></div>
                          </div>
                                <button type="submit" class="btn btn-success">save</button>
                                
                            </form>
                        </div>
                    </div>                   
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
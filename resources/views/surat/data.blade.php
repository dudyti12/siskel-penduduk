@extends('layouts.master')

@section('title','Sistem Kelurahan')

@section('search')
<div class="form-inline">
                            <form action="" method="GET" class="search-form">
                            {{ @csrf_field() }}
                                <input name = "nama_warga" class="form-control mr-sm-2" type="text" placeholder="Cari ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Surat keluar</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Surat</li>
                </ol>
            </div>
        </div>
    </div>
</div>



@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="sufee-alert alert with-close alert-info alert-dismissible fade show">
              <span class="badge badge-pill badge-info">Sukses!</span>  <p>{{ session('status') }}</p>
              
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                      
                    </div>
                   
                    
                    <div class="card pull-right">
                            
                            <form  action="/surat/search" method="get">
                            <input type="text" name="search" placeholder="Cari Data keperluan.." value="{{old('search')}}">
                            <input class="btn btn-primary btn-sm pull-right" type="submit" value="search">
                            
                            </form>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('surat')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Refresh
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Perihal</th>
                           
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                                    @foreach ($warga as $key => $item)
                             
                                <tr>    
                                   
                                    <td><strong>{{$warga->firstitem() + $key }}</strong></td>
                                    <td><i>{{$item->nama_warga}}</i></td>
                                    <td><i>{{$item->surat->perihal}}</i></td>
                                  
                                  
                                    <td class="text-center">
                                    <a href="{{'/surat/add/'.$item->id}}" class="btn btn-success btn-sm">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    <a href="{{'/surat/cetak/'.$item->id}}" class="btn btn-warning btn-sm">
                                            <i class="fa fa-print"></i>
                                        </a>
                                    <a href="" class="btn btn-warning btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>


                                     
                                     

                                    </td>

                                    @endforeach
                                </tr>
                            
                        </tbody>
                  </table>
                  <div class="pull-right">
                   <i>Showing</i>
                  {{$warga->firstItem()}} 
                  <i>to</i>
                     {{$warga->lastItem()}} 
                    <i>of</i>
                    {{$warga->total()}} 
                     <i>Entries</i>
                  </div> 
                  {{ $warga->links() }}
                  
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@endsection
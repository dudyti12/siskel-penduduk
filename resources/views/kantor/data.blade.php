@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                       <strong>Data Perkantoran</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('kantor/create')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i>Tambah
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>No.</th>
                            <th>Kategori</th>
                            <th>Alamat</th>
                            <th>Nama Kantor</th>
                            <th>Telp</th>
                            <th>Kordinat</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                                @foreach ($data_perkantoran as $key => $item)
                                <tr>    
                                    <strong><td>{{$data_perkantoran->firstitem() + $key}}</td></strong>
                                    <td>{{$item->kategori_kantor}}</td>
                                    <td>{{$item->alamat_kantor}}</td>
                                    <td>{{$item->nama_kantor}}</td>
                                    <td>{{$item->no_telp}}</td>
                                    <td>{{$item->koordinat}}</td>
                                    <td>{{$item->status}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/kantor/edit/'.$item->id_kantor)}}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{url('/kantor/delete/'.$item->id_kantor)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <div class="i fa fa-trash"></div>

                                            </button>


                                        </form>

                                    </td>

                                    
                                </tr>
                                @endforeach
                        </tbody>
                  </table>
                  {{ $data_perkantoran->links() }}
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
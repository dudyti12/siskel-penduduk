@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Perkantoran</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
       
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                       <strong>Tambah Data Perkantoran</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('kantor')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{url('kantor/store')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Kategori Kantor</label>
                                    <input type="text" name="kategori_kantor" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label>Nama Kantor</label>
                                    <input type="text" name="nama_kantor" class="form-control"  required>
                                </div>
                                <div class="form-group">
                                    <label>Alamat Kantor</label>
                                    <input type="text" name="alamat_kantor" class="form-control"  required>
                                </div>
                                <div class="form-group">
                                    <label>No Telp</label>
                                    <input type="text" name="no_telp" class="form-control"  required>
                                </div>
                                <div class="form-group">
                                    <label>Kordinat</label>
                                    <input type="text" name="koordinat" class="form-control"  required>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <input type="text" name="status" class="form-control"  required>
                                </div>
                                
                                <button type="submit" class="btn btn-success">save</button>
                            </form>

                        </div>


                    </div>
                    
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
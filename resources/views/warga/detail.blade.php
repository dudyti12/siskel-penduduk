@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')


@endsection






@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
    <div class="animated fadeIn">

                    <div class="col-lg-12 col-md-6">
                        <aside class="profile-nav alt">
                            <section class="card">
                                <div class="card-header user-header alt bg-dark">
                                    <div class="media">
                                        <a href="#">
                                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                                        </a>
                                        <div class="media-body">
                                            <h2 class="text-light display-6">{{$warga->nama_warga}}</h2>
                                            <p>Penduduk </p>
                                        </div>
                                        <div class="pull-right">
                        <a href="{{url('warga')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-caret-square-o-left"></i>Kembali
                        </a>
                    </div>
                                    </div>
                                </div>


                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <a><span class="badge badge-danger pull-left" >Nama Lengkap   :  {{$warga->nama_warga}}</span></a> 
                                    </li>
                                    
                                    <li class="list-group-item">
                                        <a> <i class="fa fa-archive"></i> NIK : <span class="badge badge-primary ">{{$warga->nik}}</span></a>
                                    </li>
                                    <li class="list-group-item ">
                                    <a> <i class="fa fa-file-text"></i> No.KK : <span class="badge badge-warning ">{{$warga->kk->no_kk}}</span></a>
                                    </li>
                                    <li class="list-group-item ">
                                    <a> <i class="fa fa-file-text"></i> Tanggal / Tempat Lahir  : <span class="badge badge-success ">{{$warga->tempat_lahir}}</span><i>,<span class="badge badge-success ">{{$warga->tanggal_lahir}}</span></i></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-road"></i> Alamat: <span class="badge badge-success ">{{$warga->kk->alamat_tinggal}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-male"></i> Jenis kelamin : <span class="badge badge-success ">{{$warga->kelamin->jenis_kelamin}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-suitcase"></i> Pekerjaan : <span class="badge badge-success ">{{$warga->pekerjaan}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-tint"></i> Golongan Darah : <span class="badge badge-success ">{{$warga->goldar->golongan_darah}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-pagelines"></i> Agama : <span class="badge badge-success p">{{$warga->agama->nama_agama}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-users"></i> Nama Kepala Keluarga : <span class="badge badge-success ">{{$warga->kk->nama_kepala_keluarga}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-home"></i> No.Akta Lahir : <span class="badge badge-success ">{{$warga->no_akta_lahir}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-flag"></i> No.Akta Kawin : <span class="badge badge-success ">{{$warga->no_akta_kawin}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-rocket"></i> Status Menikah : <span class="badge badge-success ">{{$warga->detail->status_nikah}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-star"></i> Nama Ayah : <span class="badge badge-success ">{{$warga->nama_ayah}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-star"></i> Nama Ibu : <span class="badge badge-success ">{{$warga->nama_ibu}}</span></a>
                                    </li>
                        
                                </ul>
                                    
                            </section>
                        </aside>
                    </div>




           
</div>
</div>
@endsection
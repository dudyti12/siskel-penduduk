@extends('layouts.master')
@section('title','Sistem Kelurahan')
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">  
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left"><strong>Buat Surat++</strong></div>
                    <div class="pull-right">
                        <a href="{{url('warga')}}" class="btn btn-danger btn-sm">
                            <i class="fa fa-chevron-left"></i>Batal
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{url('surat/store')}}" method="POST">
             
                                @csrf
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" disabled value="{{old('nama_warga', $wargas->nama_warga)}}" class="form-control " disabled value="{{old('nama_warga')}}" autofocus >
                                   
                                </div>
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" name="nik" disabled value="{{old('nik', $wargas->nik)}}" class="form-control " disabled value="{{old('nik')}}" autofocus >
                                   
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <input type="text" name="jenis_kel" disabled value="{{old('id_jenis_kelamin', $wargas->kelamin->jenis_kelamin)}}" class="form-control " disabled value="{{old('id_jenis_kelamin')}}" autofocus >
                                </div>
                                <div class="form-group">
                                    <label>Agama</label>
                                    <input type="text" name="agama" disabled value="{{old('id_agama', $wargas->agama->nama_agama)}}" class="form-control " disabled value="{{old('id_agama')}}" autofocus >
                                </div>
                                <div class="form-group">
                                    <label>Status Perkawinan</label>
                                    <input type="text" name="status" disabled value="{{old('id_kawin', $wargas->detail->status_nikah)}}" class="form-control " disabled value="{{old('id_kawin')}}" autofocus >
                                </div>
                                <div class="form-group">
                                    <label>Pekerjaan</label>
                                    <input type="text" name="pekerjaan" disabled value="{{old('pekerjaan', $wargas->pekerjaan)}}" class="form-control " disabled value="{{old('pekerjaan')}}" autofocus >
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="alamat" disabled value="{{old('alamat', $wargas->kk->alamat_tinggal)}}" class="form-control " disabled value="{{old('alamat_tinggal')}}" autofocus >
                                </div>
                                <div class="form-group">
                                    <label>Nomor Surat</label>
                                    <input type="text" name="nomor"  value="{{old('nomor', $wargas->nomor)}}" class="form-control @error('nomor') is-invalid @enderror" value="{{old('nomor')}}" required >
                                  
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Tanggal Surat</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input name="tanggal_surat" class="form-control" value="{{old('tanggal_surat')}}">
                                       
                                    </div>
                                   
                                </div>  
                                <div class="row form-group">
                            <div class="col col-md-1"><label for="textarea-input" class=" form-control-label">isi</label></div>
                            <div class="col-12 col-md-12"><textarea name="perihal" id="id" rows="9" placeholder="Isi Surat.." class="form-control"></textarea></div>
                             </div>
                               
                                <button type="submit" class="btn btn-success">Buat</button>
                                
                            </form>
                        </div>
                    </div>                   
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
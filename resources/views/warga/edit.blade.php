@extends('layouts.master')
@section('title','Sistem Kelurahan')
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">  
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left"><strong>Edit Data Warga</strong></div>
                    <div class="pull-right">
                        <a href="{{url('warga')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{url('warga/'.$wargas->id)}}" method="POST">
                            @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama_warga" value="{{old('nama_warga', $wargas->nama_warga)}}" class="form-control @error('nama_warga') is-invalid @enderror" value="{{old('nama_warga')}}" autofocus >
                                    @error('nama_warga')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Pendidikan</label>
                                    <select type="text" name="id_pendidikan"   class="form-control @error('id_pendidikan') is-invalid @enderror" >
                                    <option value="">--Pilih Pendidikan</option>
                                    @foreach($pendidikan as $item)
                                        <option value="{{$item->id}}" {{old('id_pendidikan', $wargas->id_pendidikan) == $item->id ? 'selected':null}}>{{$item->nama_pendidikan}}</option>
                                    @endforeach
                                    </select>
                                    @error('id_pendidikan')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="select" class=" form-control-label @error('id_jenis_kelamin') is-invalid @enderror">Jenis Kelamin</label></div>
                                    <div class="col-12 col-md-9">
                                      <select  name="id_jenis_kelamin" id="select" class="form-control">
                                      <option value="">--Pilih Jenis Kelamin</option>
                                      @foreach($kelamin as $item)
                                      <option value="{{$item->id}}" {{old('id_jenis_kelamin',$wargas->id_jenis_kelamin) == $item->id ? 'selected':null}}>{{$item->jenis_kelamin}}</option>
                                      @endforeach
                                      </select>
                                      @error('id_jenis_kelamin')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label>Golongan Darah</label>
                                    <select type="text" name="id_goldar"  class="form-control @error('id_goldar') is-invalid @enderror" >
                                    <option value="">--Pilih Golongan Darah</option>
                                    @foreach($goldar as $item)
                                        <option value="{{$item->id}}" {{old('id_goldar', $wargas->id_goldar) == $item->id ? 'selected':null}}>{{$item->golongan_darah}}</option>
                                    @endforeach
                                    </select>
                                    @error('id_goldar')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Nama Kepala Keluarga</label>
                                    <select type="text" name="id_kepalakeluarga"  class="form-control @error('id_kepalakeluarga') is-invalid @enderror" >
                                    <option value="">--Pilih Kepala Keluarga</option>
                                    @foreach($kk as $item)
                                        <option value="{{$item->id}}" {{old('id_kepalakeluarga',$wargas->id_kepalakeluarga) == $item->id ? 'selected':null}}>{{$item->nama_kepala_keluarga}}</option>
                                    @endforeach
                                    </select>
                                    @error('id_kepalakeluarga')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select type="text" name="id_agama"  class="form-control @error('id_agama') is-invalid @enderror" >
                                    <option value="">--Pilih Agama</option>
                                    @foreach($agama as $item)
                                    <option value="{{$item->id}}" {{old('id_agama',$wargas->id_agama) == $item->id ? 'selected':null}}>{{$item->nama_agama}}</option>
                                    @endforeach
                                    </select>
                                    @error('id_agama')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>  
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" name="nik" value="{{old('nik', $wargas->nik)}}"  class="form-control @error('nik') is-invalid @enderror" value="{{old('nik')}}">
                                    @error('nik')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Tanggal Lahir</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input name="tanggal_lahir" class="form-control @error('tanggal_lahir') is-invalid @enderror" value="{{old('tanggal_lahir',$wargas->tanggal_lahir)}}">
                                    </div>
                                    <strong class="form-text text-muted">Format: Tahun/Bulan/Tanggal | contoh: 1997-03-23</strong>
                                    @error('tanggal_lahir')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>    
                                <div class="form-group">
                                    <label>Pekerjaan</label>
                                    <input type="text" name="pekerjaan" value="{{old('pekerjaan', $wargas->pekerjaan)}}"  class="form-control @error('pekerjaan') is-invalid @enderror" value="{{old('pekerjaan')}}">
                                    @error('pekerjaan')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>   
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" value="{{old('tempat_lahir' ,$wargas->tempat_lahir)}}"  class="form-control @error('tempat_lahir') is-invalid @enderror" value="{{old('tempat_lahir')}}">
                                    @error('tempat_lahir')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                  <div class="row form-group">
                                    <div class="col col-md-3"><label for="select" class=" form-control-label">Status Kawin</label></div>
                                    <div class="col-12 col-md-9">
                                      <select name="id_kawin" id="select" class="form-control  @error('id_kawin') is-invalid @enderror">
                                        <option value="">--Pilih Status Pernikahan</option>
                                        @foreach($detail as $item)
                                        <option value="{{$item->id}}" {{old('id_kawin',$wargas->id_kawin) == $item->id ? 'selected':null}}>{{$item->status_nikah}}</option>
                                        @endforeach     
                                      </select>
                                    </div>
                                  </div>                               
                                <div class="form-group">
                                    <label>No Akta Lahir</label>
                                    <input type="text" name="no_akta_lahir"   class="form-control @error('no_akta_lahir') is-invalid @enderror" value="{{old('no_akta_lahir' , $wargas->no_akta_lahir)}}"">
                                    @error('no_akta_lahir')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>No Akta Kawin</label>
                                    <input type="text" name="no_akta_kawin"   class="form-control @error('no_akta_kawin') is-invalid @enderror" value="{{old('no_akta_kawin', $wargas->no_akta_kawin)}}"">
                                    @error('no_akta_kawin')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                                </div>
                                <div class="form-group">
                                    <label>Nama Ayah</label>
                                    <input type="text" name="nama_ayah" value="{{old('nama_ayah', $wargas->nama_ayah)}}"  class="form-control @error('nama_ayah') is-invalid @enderror" value="{{old('nama_ayah')}}">
                                    @error('nama_ayah')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                                </div>
                                <div class="form-group">
                                    <label>Nama Ibu</label>
                                    <input type="text" name="nama_ibu" value="{{old('nama_ibu', $wargas->nama_ibu)}}"  class="form-control @error('nama_ibu') is-invalid @enderror" value="{{old('nama_ibu')}}">
                                    @error('nama_ibu')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                                </div>
                                <button type="submit" class="btn btn-success">save</button>
                                
                            </form>
                        </div>
                    </div>                   
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
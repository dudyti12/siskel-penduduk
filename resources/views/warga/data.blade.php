@extends('layouts.master')

@section('title','Sistem Kelurahan')

@section('search')
<div class="form-inline">
                            <form action="{{ url('warga') }}" method="GET" class="search-form">
                            {{ @csrf_field() }}
                                <input name = "nama_warga" class="form-control mr-sm-2" type="text" placeholder="Cari ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>



@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="sufee-alert alert with-close alert-info alert-dismissible fade show">
              <span class="badge badge-pill badge-info">Sukses!</span>  <p>{{ session('status') }}</p>
              
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                      
                    </div>
                    <div class="pull-right">
                        <a href="{{url('warga/create')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i>Tambah
                        </a>
                    </div>
                    
                    <div class="card pull-left">
                            
                            <form  action="/warga/cari" method="get">
                            <input type="text" name="cari" placeholder="Ketik nama warga.." value="{{old('cari')}}">
                            <input class="btn btn-primary btn-sm pull-right" type="submit" value="cari">
                            
                            </form>
                    </div>
                    <div class="pull-left">
                        <a href="{{url('warga')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Refresh
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            
                            <th>NIK</th>
                            <th>Jenis Kelamin</th>
                            <th>Nama Kepala Keluarga</th>
                         

                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                                @foreach ($wargas as $key => $item)
                                <tr>    
                                   
                                    <td><strong>{{$wargas->firstitem() + $key }}</strong></td>
                                    <td><i>{{$item->nama_warga}}</i></td>
                                    <td><i>{{$item->nik}}</i></td>
                                    <td><i>{{$item->kelamin->jenis_kelamin}}</i></td>
                                    <td><i>{{$item->kk->nama_kepala_keluarga}}</i></td>
                                    <td class="text-center">
                                    
                                    <a href="{{url('warga/'.$item->id)}}" class="btn btn-warning btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                      


                                        <a href="{{url('warga/edit/'.$item->id)}}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{url('warga/delete/'.$item->id)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <div class="i fa fa-trash"></div>

                                            </button>


                                        </form>

                                    </td>

                                    
                                </tr>
                                @endforeach
                        </tbody>
                  </table>
                  <div class="pull-right">
                   <i>Showing</i>
                  {{$wargas->firstItem()}} 
                  <i>to</i>
                     {{$wargas->lastItem()}} 
                    <i>of</i>
                    {{$wargas->total()}} 
                     <i>Entries</i>
                  </div> 
                  {{ $wargas->links() }}
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@endsection
@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
               
             
                            

                <div class="content mt-3">
            <div class="animated fadeIn">
            <div class="pull-right">
                        <a href="{{url('edulevel')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-back"></i>Kembali
                        </a>
                    </div>
                <div class="row ">
                    <div class="col-lg-6">
                        <div class="card pull-right">
                            <div class="card-header col-12">
                                <h4>Detail Jenis Program</h4>
                            </div>
                            <div class="card-body center">
                               
                               
                                <ul class="list-unstyled">
                                 
                                <li><p>ID:</p>{{$program->id}}</li>
                                <li><p>Nama Program:</p>{{$program->name}}</li>
                                <li><p>Harga:</p>{{$program->student_price}}</li>
                                <li><p>Maksimum Peserta:</p>{{$program->student_max}}</li>
                                <li><p>Level:</p>{{$program->edulevel->name}}</li>
                                <li><p>Deskripsi:</p>{{$program->info}}</li>
                                
                                    
                                  
                                </ul>
                     
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
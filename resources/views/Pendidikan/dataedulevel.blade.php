@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                       <strong>Data Warga</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('penduduk/create')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i>Tambah
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                       
                            <th>Id</th>
                            <th>nama</th>
                            <th>Students Price</th>
                            <th>Deskripsi</th>
                         
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                                @foreach ($edulevel as $item)
                                <tr>    
                                   
                                  
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->desc}}</td>
                                    <td>{{$item->program->name}}</td>
                                   
                               
                                  
                                    
                                    <td class="text-center">
                                    <a href="{{url('program/'.$item->id)}}" class="btn btn-warning btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{url('pendidikan/edit/'.$item->id_pendidikan)}}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{url('pendidikan/delete/'.$item->id_pendidikan)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <div class="i fa fa-trash"></div>

                                            </button>


                                        </form>

                                    </td>

                                    
                                </tr>
                                @endforeach
                        </tbody>
                  </table>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
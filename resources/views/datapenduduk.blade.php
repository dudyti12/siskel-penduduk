@extends('layouts.app')

@section('content')

<p>Info Penduduk</p>
<p>No                :{{$data_warga->iddata_warga}}</p>
<p>Nama              :{{$data_warga->nama_warga}}</p>
<p>tanggal Lahir     :{{$data_warga->nama_warga}}</p>
<p>Jenis Kelamin     :{{$data_warga->jenis_kelamin}}</p>
<p>Agama             :{{$data_warga->agama}}</p>
<p>Golongan Darah    :{{$data_warga->agama}}</p>
<p>Pendidikan        :{{$data_warga->gol_darah}}</p>
<p>Pekerjaan         :{{$data_warga->pekerjaan}}</p>
<p>No Akta Lahir     :{{$data_warga->no_akta_lahir}}</p>
<p>No Akta Kawin     :{{$data_warga->no_akta_kawin}}</p>
<p>Tempat Lahir      :{{$data_warga->tempat_lahir}}</p>
<p>Status Kawin      :{{$data_warga->status_kawin}}</p>
<p>Nama Ayah         :{{$data_warga->nama_ayah}}</p>
<p>Nama Ibu          :{{$data_warga->nama_ibu}}</p>

<div>
    <a href="/beranda" class="btn btn-sm btn-primary ">Kembali</a>
</div>

@endsection
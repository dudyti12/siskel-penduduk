@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Surat</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                       <strong>Tambah Kategori Surat</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('surat')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{url('surat/simpan')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Nomor Surat</label>
                                    <input type="text" name="nomor" value="{{old('nomor')}}" class="form-control @error('nomor') is-invalid @enderror" value="{{old('nomor')}}" autofocus >
                                    @error('nomor')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Jenis Surat</label>
                                    <input type="text" name="perihal" value="{{old('perihal')}}" class="form-control @error('perihal') is-invalid @enderror" value="{{old('perihal')}}" autofocus >
                                    @error('perihal')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                              
                                <button type="submit" class="btn btn-success">save</button>
                                </div>
                            </form>

                        </div>


                    </div>
                    
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
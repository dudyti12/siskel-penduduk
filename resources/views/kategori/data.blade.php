@extends('layouts.master')

@section('title','Sistem Kelurahan')

@section('search')
<div class="form-inline">
                            <form action="" method="GET" class="search-form">
                            {{ @csrf_field() }}
                                <input name = "nama_warga" class="form-control mr-sm-2" type="text" placeholder="Cari ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Buat Kategori Surat</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Surat</li>
                </ol>
            </div>
        </div>
    </div>
</div>



@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="sufee-alert alert with-close alert-info alert-dismissible fade show">
              <span class="badge badge-pill badge-info">Sukses!</span>  <p>{{ session('status') }}</p>
              
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    
                   Buat Kategori Surat: Nomor dan Jenis Surat!
                   <div class="pull-right">
                        <a href="{{url('surat/tambah')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i>Tambah
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                          
                            <th>No.</th>
                            <th>Nomor</th>
                            <th>Jenis Surat</th>
                           
                            <th>Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                                   
                        @foreach ($surats as $key => $item)
                                <tr>    
                                   
                                    <td><strong>{{$surats->firstitem() + $key }}</strong></td>
                                    <td><i>{{$item->nomor}}</i></td>
                                    <td><i>{{$item->perihal}}</i></td>
                                  
                                  
                                    <td class="text-center">
                                    <a href="" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{url('/surat/hapus/'.$item->id)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <div class="i fa fa-trash"></div>

                                            </button>


                                        </form>


                                     
                                     

                                    </td>

                                  
                                </tr>
                            @endforeach
                        </tbody>
                  </table>
                  <div class="pull-right">
                   <i>Showing</i>
                   {{$surats->firstItem()}} 
                  <i>to</i>
                 
                    <i>of</i>
                 
                     <i>Entries</i>
                  </div>      
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@endsection
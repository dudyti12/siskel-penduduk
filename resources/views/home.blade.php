@extends('layouts.master')

@section('title','Sistem Kelurahan')

@section('search')

@section('breadcrumbs')
<div class="alert alert-warning" role="alert">
 <h4 class="alert-heading">Selamat Datang Admin!</h4>
<hr>
<p> Harap untuk mengikuti panduan penggunan SI-PEN berdasarkan aturan sebagai berikut:</p>
<li>Aplikasi Ini merupakan v.0.1, apabila terdapat beberapa fitur yang belum berfungsi dengan baik dikarenakan masih tahap pengembangan.</li> 
   <li>Mohon Untuk Mengisi Data Keluarga Terlebih Dahulu.</li> 
   <li>Pastikan Anda Mengecek dengan seksama setiap data yang hendak  anda hapus!.</li> 
   <p>Terima kasih! Have a nice Day:)</p>
</div>




@endsection

@section('content')


<div class="col-xl-3 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total Warga</div>
                                <div class="stat-digit">{{$count}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Warga Pria</div>
                                <div class="stat-digit">{{$laki}}</div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Warga Wanita</div>
                                <div class="stat-digit">{{$cewek}}</div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Keluarga</div>
                                <div class="stat-digit">{{$kk}}</div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-xl-3 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Fasilitas Umum</div>
                                <div class="stat-digit">{{$fasum}}</div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total Kantor</div>
                                <div class="stat-digit">{{$kantor}}</div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
                                       


@endsection
@endsection
@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Keluarga</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                    <strong>Tambah Data Keluarga</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('kk')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="class col-md-4 offset-md-4">
                            <form action="{{url('kk/'.$kks->id)}}" method="POST">
                            @method('PUT')
                            @csrf                   
                                <div class="form-group" >
                                    <label>Nama Kepala Keluarga</label>
                                    <input type="text" name="nama_kepala_keluarga" value="{{old('nama_kepala_keluarga', $kks->nama_kepala_keluarga)}}" class="form-control @error('nama_kepala_keluarga') is-invalid @enderror" value="{{old('nama_kepala_keluarga')}}" autofocus>
                                    @error('nama_kepala_keluarga')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>            
                                <div class="form-group">
                                    <label>No KK</label>
                                    <input type="text" name="no_kk" value="{{old('no_kk', $kks->no_kk)}}" class="form-control @error('no_kk') is-invalid @enderror" value="{{old('no_kk')}}">
                                    @error('no_kk')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>  
                                <div class="form-group">
                                    <label>Jumlah Anggota Keluarga</label>
                                    <input type="number" name="jumlah_anggota_klg" value="{{old('jumlah_anggota_klg', $kks->jumlah_anggota_klg)}}" class="form-control @error('jumlah_anggota_klg') is-invalid @enderror" value="{{old('jumlah_anggota_klg')}}">
                                    @error('jumlah_anggota_klg')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>   
                                <div class="form-group">
                                    <label>Alamat Tinggal</label>
                                    <input type="text" name="alamat_tinggal" value="{{old('alamat_tinggal', $kks->alamat_tinggal)}}" class="form-control @error('alamat_tinggal') is-invalid @enderror" value="{{old('alamat_tinggal')}}">
                                    @error('alamat_tinggal')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Rukun Tetangga</label>
                                    <input type="number" name="rukun_tetangga" value="{{old('rukun_tetangga', $kks->rukun_tetangga)}}" class="form-control @error('rukun_tetangga') is-invalid @enderror" value="{{old('rukun_tetangga')}}">
                                    @error('rukun_tetangga')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Rukun Warga</label>
                                    <input type="number" name="rukun_warga" value="{{old('rukun_warga', $kks->rukun_warga)}}" class="form-control @error('rukun_warga') is-invalid @enderror" value="{{old('rukun_warga')}}">
                                    @error('rukun_warga')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Kelurahan</label>
                                    <input type="text" name="kelurahan" value="{{old('kelurahan', $kks->kelurahan)}}" class="form-control @error('kelurahan') is-invalid @enderror" value="{{old('kelurahan')}}">
                                    @error('kelurahan')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <input type="text" name="kecamatan" value="{{old('kecamatan', $kks->kecamatan)}}" class="form-control @error('kecamatan') is-invalid @enderror" value="{{old('kecamatan')}}">
                                    @error('kecamatan')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-success">save</button>
                                </div>
                                
                            </form>

                        </div>


                    </div>
                    
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
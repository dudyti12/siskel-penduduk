@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="page-header float-right">
        <div class="pull-right">
                        <a href="{{url('kk')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-caret-square-o-left"></i>Kembali
                        </a>
                    </div>
            
        </div>
    </div>
</div>

@endsection






@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
    <div class="animated fadeIn">

                    <div class="col-lg-12 col-md-6">
                        <aside class="profile-nav alt">
                            <section class="card">
                                <div class="card-header user-header alt bg-dark">
                                    <div class="media">
                                        <a href="#">
                                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                                        </a>
                                        <div class="media-body">
                                            <h2 class="text-light display-6">{{$kk->nama_kepala_keluarga}}</h2>
                                            <p>Penduduk </p>
                                        </div>
                                    </div>
                                </div>


                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <a><span class="badge badge-danger pull-left" >Nama Lengkap   :  {{$kk->nama_kepala_keluarga}}</span></a> 
                                    </li>
                                    <li class="list-group-item ">
                                    <a> <i class="fa fa-file-text"></i> No.KK : <span class="badge badge-warning ">{{$kk->no_kk}}</span></a>
                                    </li>
                                    <li class="list-group-item ">
                                    <a> <i class="fa fa-file-text"></i> Jumlah Anggota Keluarga  : <span class="badge badge-success ">{{$kk->jumlah_anggota_klg}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-road"></i> Rukun Tetangga: <span class="badge badge-success ">{{$kk->rukun_tetangga}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-male"></i> Rukun warga : <span class="badge badge-success ">{{$kk->rukun_warga}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-suitcase"></i> Alamat  : <span class="badge badge-success ">{{$kk->alamat_tinggal}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-tint"></i> Kecamatan: <span class="badge badge-success ">{{$kk->kecamatan}}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                    <a> <i class="fa fa-pagelines"></i> Kelurahan : <span class="badge badge-success p">{{$kk->kelurahan}}</span></a>
                                    </li>
                                </ul>

                            </section>
                        </aside>
                    </div>




           
</div>
</div>
@endsection
@extends('layouts.master')

@section('title','Sistem Kelurahan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Penduduk</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 {{--<li><a href="#">Dashboard</a></li>--}}   
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
    <div class="animated fadeIn">
            <div class="card">
                <div class="div card-header">
                    <div class="pull-left">
                       <strong>Data Keluarga</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('kk/create')}}" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i>Tambah
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>No.</th>
                            <th>Nama Kepala Keluarga</th>
                            <th>KK</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                                @foreach ($kk as $key => $item)
                                <tr>    
                                    <td><strong>{{$kk->firstitem() + $key}}</strong></td>
                                    <td>{{$item->nama_kepala_keluarga}}</td>
                                    <td>{{$item->no_kk}}</td>
                                    <td class="text-center">
                                    <a href="{{url('/kk/'.$item->id)}}" class="btn btn-warning btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>


                                        <a href="{{url('/kk/'.$item->id.'/edit')}}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{url('/kk/'.$item->id)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <div class="i fa fa-trash"></div>

                                            </button>


                                        </form>

                                    </td>

                                    
                                </tr>
                                @endforeach
                        </tbody>
                  </table>
                  <div class="pull-right">
                   <i>Showing</i>
                  {{$kk->firstItem()}} 
                  <i>to</i>
                     {{$kk->lastItem()}} 
                    <i>of</i>
                    {{$kk->total()}} 
                     <i>Entries</i>
                  </div> 
                </div>
                {{$kk->links() }}
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
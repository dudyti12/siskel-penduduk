<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::get('home', 'HomeController@index')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');
    //ROUTE DATA WARGA-------------------------------------------------
Route::get('/beranda','HomeController@index')->name('beranda');
Route::get('/warga','WargaController@show');
Route::get('/warga/create','WargaController@create');
Route::post('/warga/store','WargaController@store');
//Route::delete('/penduduk/lihat/{id}','PendudukController@destroy');
Route::get('/warga/edit/{id}','WargaController@edit');
//Route::patch('/penduduk/{id}','PendudukController@store');
Route::put('/warga/update/{id}','WargaController@update');
Route::delete('/warga/delete/{id}','WargaController@destroy');
Route::get('/warga/cari','WargaController@cari');
//Route::get('/penduduk/utama','PendudukController@utama');
//Route::resource('penduduk', 'PendudukController');
//Route::resource('/keluarga', 'KeluargaController');
Route::resource('warga','WargaController');
Route::resource('kelamin','KelaminController');

// Route::get('print','WargaController@print');
// Route::get('print/create','PrintController@create');
// Route::get('cetak','PrintController@create');
// Route::get('htmltopdfview',array('as'=>'htmltopdfview','uses'=>'PrintController@htmltopdfview'));

//ROUTE DATA KK---------------------------------------------------
 Route::get('/kk','KkController@index');
// Route::get('/keluarga/create','KeluargaController@create');
 Route::post('/kk/store','KkController@store');
// Route::get('/keluarga/edit/{id}','KeluargaController@edit');
 Route::patch('/kk/{id}','Kk@update');
// Route::delete('/keluarga/delete/{id}','KeluargaController@destroy');
 Route::resource('kk','KkController');
//ROUTE DATA FASUM---------------------------------------------------
Route::get('/fasum','FasumController@show');
Route::get('/fasum/create','FasumController@create');
Route::post('/fasum/store','FasumController@store');
Route::get('/fasum/edit/{id}','FasumController@edit');
Route::patch('/fasum/{id}','FasumController@update');
Route::delete('/fasum/delete/{id}','FasumController@destroy');
//ROUTE DATA PERKANTORAN---------------------------------------------------
Route::get('/kantor','KantorController@show');
Route::get('/kantor/create','KantorController@create');
Route::post('/kantor/store','KantorController@store');
Route::get('/kantor/edit/{id}','KantorController@edit');
Route::patch('/kantor/{id}','KantorController@update');
Route::delete('/kantor/delete/{id}','KantorController@destroy');
//NUMPANG DULU!----------------------------------------------------------------------------
Route::get('/berkas','KantorController@berkas');
Route::post('/berkas/proses', 'KantorController@proses_upload');
//Pendidikan----------------------------------------------------------------------------
// Route::get('program', 'ProgramController@show');
Route::get('edulevel', 'EdulevelController@show');
Route::resource('program','ProgramController');
//Pendidikan-2---------------------------------------------------------------------------
Route::resource('pendidikan','PendidikanController');

Route::get('/gambar','GambarController@show');
Route::post('/gambar/create', 'GambarController@create');
Route::delete('/gambar/delete/{id}', 'GambarController@destroy');
// Route::get('/gambar/{id}', 'GambarController@getFile');
Route::get('print','PrintController@index');
//  Route::get('/print/create', 'PrintController@create');
 Route::post('/print/store','PrintController@store');
 Route::get('/print/edit/{id}','PrintController@edit');
 Route::patch('/print/{id}','PrintController@update');
 Route::delete('/print/delete/{id}','PrintController@destroy');
 Route::get('print/detail/{id}', 'PrintController@show');
 Route::get('cetak/{$id}','WargaController@cetak');
 Route::get('surat','SuratController@index');
Route::get('/surat/create','SuratController@create');
Route::get('/surat/search','SuratController@search');
Route::post('surat/store','SuratController@store');
Route::get('mail/{id}','SuratController@mail' );
Route::put('/surat/update/{id}','SuratController@updatemail');
Route::get('surat/add/{id}','SuratController@tampil');
Route::get('/surat/store','SuratController@store');
Route::get('/surat/test','SuratController@lempar');
Route::get('/surat/cetak/{id}','SuratController@proses');
Route::get('/surat/test/','SuratController@cetak');
Route::get('/surat/show','SuratController@show');
Route::get('/surat/tambah','SuratController@tambah');
Route::post('/surat/simpan','SuratController@simpan');
Route::delete('/surat/hapus/{id}','SuratController@hapus');
Route::get('kategori','SuratController@show');
//cobaaaa-----
Route::get('home/cekumur/','HomeController@cekumur');
});

Auth::routes();

Route::get('/', function () {
    return view('login');
});




<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data_fasum extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    
    protected $primaryKey = 'iddata_fasum';
    protected $fillable = [
    ];

    public function setUpdatedAt($value)
 {
   return NULL;
 }


 public function setCreatedAt($value)
 {
   return NULL;
 }

    public function user(){

       return $this->belongsTo('App\Models\User');
   }
   protected $table = 'data_fasum';
}

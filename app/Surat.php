<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    //
    public $timestamps = false;
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $table = 'surats';


    public function warga(){
        return $this->hasOne('App\Warga');

    }

    public function agama(){

        return $this->hasOne('App\Agama');

    }    

    
}

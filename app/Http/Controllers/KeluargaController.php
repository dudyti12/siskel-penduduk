<?php

namespace App\Http\Controllers;

use App\Models\Data_kk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class KeluargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('keluarga.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
        DB::table('data_kk')->insert([
            'no_kk' => $request->no_kk,
            'nama_kepala_keluarga' => $request->nama_kepala_keluarga,
            'jumlah_anggota_klg' => $request->jumlah_anggota_klg,
            'rukun_tetangga' => $request->rukun_tetangga,
            'rukun_warga' => $request->rukun_warga,
            'alamat_tinggal' => $request->alamat_tinggal,
            'kecamatan' => $request->kecamatan,
            'kelurahan' => $request->kelurahan,
            ]);

            return redirect('keluarga')->with('status','Data KK Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Data_kk  $data_kk
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $data_kk = DB::table('data_kk')->get();
         //dd($data_kk);
       return view('keluarga.data',compact('data_kk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Data_kk  $data_kk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data_kk = DB::table('data_kk')->where('iddata_keluarga', $id)->first();;
       
       //dd($data_warga);
       return view('keluarga.edit',compact('data_kk'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Data_kk  $data_kk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('data_kk')->where('iddata_keluarga',$id )
        ->update([
          'no_kk' => $request->no_kk,
          'nama_kepala_keluarga' => $request->nama_kepala_keluarga,
          'jumlah_anggota_klg' => $request->jumlah_anggota_klg,
          'rukun_tetangga' => $request->rukun_tetangga,
          'rukun_warga' => $request->rukun_warga,
          'alamat_tinggal' => $request->alamat_tinggal,
          'kecamatan' => $request->kecamatan,
          'kelurahan' => $request->kelurahan, 
          ]);
          return redirect('keluarga')->with('status','Data KK Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Data_kk  $data_kk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       // return "delete";
        DB::table('data_kk')->where('iddata_keluarga', $id)->delete();
        return redirect('keluarga')->with('status','Data KK Berhasil Dihapus');

    }
}

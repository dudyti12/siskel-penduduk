<?php

namespace App\Http\Controllers;

use App\Data_perkantoran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class KantorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kantor.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data_perkantoran = DB::table('data_perkantoran')->insert([
            'kategori_kantor' => $request->kategori_kantor,
            'nama_kantor' => $request->nama_kantor,
            'alamat_kantor' => $request->alamat_kantor,
            'no_telp' => $request->no_telp,
            'koordinat' => $request->koordinat,
            'status' => $request->status,
            ]);
            return redirect('kantor')->with('status','Data Perkantoran Berhasil Ditambah!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Data_perkantoran  $data_perkantoran
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $data_perkantoran = Data_perkantoran::paginate(5);
        return view('kantor.data',compact('data_perkantoran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Data_perkantoran  $data_perkantoran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data_perkantoran=DB::table('data_perkantoran')->where('id_kantor',$id)->first();

        return view('kantor.edit',compact('data_perkantoran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Data_perkantoran  $data_perkantoran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('data_perkantoran')
              ->where('id_kantor', $id)
              ->update([
                'kategori_kantor' => $request->kategori_kantor,
                'nama_kantor' => $request->nama_kantor,
                'alamat_kantor' => $request->alamat_kantor,
                'no_telp' => $request->no_telp,
                'koordinat' => $request->koordinat,
                'status' => $request->status,
              ]);
              return redirect('kantor')->with('status','Data Perkantoran Berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  \App\Models\Data_perkantoran  $data_perkantoran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('data_perkantoran')->where('id_kantor',$id)->delete();
        return redirect('kantor')->with('status','Data Perkantoran Berhasil Dihapus!');
    }

    public function berkas(){


        $gambar = DB::table('gambar')->get();
        return view('berkas.data',compact('gambar'));

    }

    public function proses_upload(Request $request){

        $this->validate($request,[

            'file' => 'required'

        ]);
          //  dd($request);
        // menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');

        // nama file
       echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';
        // ekstensi file
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';
        // real path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';
        // ukuran file
        echo 'File Size: '.$file->getSize();
        echo '<br>';
        // tipe mime
       echo 'File Mime Type: '.$file->getMimeType();
        // isi dengan nama folder tempat kemana file diupload
       $tujuan_upload = 'data_file';
        //  upload file
       $file->move($tujuan_upload,$file->getClientOriginalName());
       return redirect('berkas')->with('status','Gambar Berhasil Disimpan!');

    }
}

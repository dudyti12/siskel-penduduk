<?php

namespace App\Http\Controllers;

use App\Warga;
use App\Agama;
use App\Detail;
use App\Pendidikan;
use App\Goldar;
use App\Kelamin;
use App\Kk;
use App\Surat;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\User;
use App\Program;

class WargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    } /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        //

        $wargas = Warga::paginate(5);
        // return $wargas;
        return view('warga.data', compact('wargas'));
        
    }


    public function cari(Request $request)
    {
        
        $cari = $request->cari;
        $wargas = Warga::where('nama_warga','like',"%".$cari."%")->Orwhere('nama_ayah','like',"%".$cari."%")->paginate(3);
        return view('warga.data',['wargas'=>$wargas]); 


    }

    public function cetak_pdf()
    {
    	$wargas = Warga::all();
 
    	$pdf = PDF::loadview('pegawai_pdf',['wargas'=>$wargas]);
    	return $pdf->download('laporan-pdf');
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       // $data_warga = Data_warga::pluck('id_jenis_kelamin', 'iddata_warga');

       // return view('creatependuduk', [
       //     'data_warga' => $data_warga,]);
        $agama = Agama::all();
        $pendidikan = Pendidikan::all();
        $kelamin = Kelamin::all();
        $goldar = Goldar::all();
        $kk = Kk::all();
        $detail = Detail::all();
        $surat = Surat::all();
        return view('warga.add',compact('agama','pendidikan','kelamin','goldar','kk','detail','surat'));
    }
    
    public function utama(){


        return view('layouts.master');
    }

    public function test(){

        
        return view('warga.test');

    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->_validate($request);
    //     DB::table('wargas')->insert([
    //         'nama_warga' => $request->nama_warga, 
    //         'nik' => $request->nik, 
    //         ]);
    //         return redirect('warga')->with('status','Data Warga Berhasil Ditambah');
            $warga = new Warga();
            $keperluan = 2;
            $warga->nama_warga = $request->nama_warga;
            $warga->nik = $request->nik;
            $warga->id_pendidikan = $request->id_pendidikan;
            $warga->id_agama = $request->id_agama;
            $warga->id_kepalakeluarga = $request->id_kepalakeluarga;
            $warga->id_goldar = $request->id_goldar;
            $warga->id_jenis_kelamin = $request->id_jenis_kelamin;
            $warga->tanggal_lahir = $request->tanggal_lahir;
            $warga->pekerjaan = $request->pekerjaan;
            $warga->no_akta_lahir = $request->no_akta_lahir;
            $warga->no_akta_kawin = $request->no_akta_kawin;
            $warga->tempat_lahir = $request->tempat_lahir;
            $warga->id_keperluan = $keperluan;
            $warga->id_kawin = $request->id_kawin;
            $warga->nama_ayah = $request->nama_ayah;
            $warga->nama_ibu = $request->nama_ibu;
           
            $warga->save();    
            // return $request;
            
            return redirect('warga')->with('status','Data Warga Berhasil Ditambah');
    
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Warga $warga)
    {
        //
      //  dd($slug);
       //$data_warga =  Data_warga::where('iddata_warga', $id)->first();
      // $id = Data_warga::where('iddata_warga', $id)->first();
    //   $warga = Warga::all();
    //   //$data_warga = DB::table('data_warga')->get();
    //  //dd($data_warga);
    //    return view('warga.data',compact('warga'));
    // $wargas = Warga::all();
    // //  return $program;
    //  return view('warga.detail',compact('wargas'));
     $warga->makehidden(['id_agama']);
    // return $warga;
    return view('warga.detail',compact('warga'));
        
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agama = Agama::all();
        $pendidikan = Pendidikan::all();
        $kelamin = Kelamin::all();
        $goldar = Goldar::all();
        $kk = Kk::all();
        $detail = Detail::all();
        $wargas = Warga::where('id', $id)->first();       
       //dd($data_warga);
        return view('warga.edit',compact('wargas','agama','pendidikan','kelamin','goldar','kk','detail'));
    }

    public function cetak($id){

       
        // $wargas = Warga::where('id',$id);  
        // return view('warga.cetak',compact('wargas'));
        return $id;
          
               

    }
/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->_validate($request);
    //    Warga::where('id', $id)->update([

    //         'nama_warga' => $request->nama_warga, 
    //         'nik' => $request->nik, 
    //    ]);
    //    return redirect('warga')->with('status','Data Warga Berhasil Diupdate');
        // return $request;
        $warga = Warga::find($id);

        $warga->nama_warga = $request->nama_warga;
        $warga->nik = $request->nik;
        $warga->id_pendidikan = $request->id_pendidikan;
        $warga->id_agama = $request->id_agama;
        $warga->id_kepalakeluarga = $request->id_kepalakeluarga;
        $warga->id_goldar = $request->id_goldar;
        $warga->id_jenis_kelamin = $request->id_jenis_kelamin;
        $warga->tanggal_lahir = $request->tanggal_lahir;
        $warga->pekerjaan = $request->pekerjaan;
        $warga->no_akta_lahir = $request->no_akta_lahir;
        $warga->no_akta_kawin = $request->no_akta_kawin;
        $warga->tempat_lahir = $request->tempat_lahir;
        $warga->id_kawin = $request->id_kawin;
        $warga->nama_ayah = $request->nama_ayah;
        $warga->nama_ibu = $request->nama_ibu;
        $warga->save();    
        return redirect('warga')->with('status','Data Warga Berhasil Diupdate');
        // return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {       
    DB::table('wargas')->where('id',$id)->delete();
    return redirect('warga')->with('status','Data Warga Berhasil Dihapus!');

    }

    public function _validate(Request $request){

        $request->validate([
            'nik' => 'required|numeric',
            'nama_warga' => 'required|max:100|min:3',
            'id_pendidikan' => 'required',
            'id_agama' => 'required',
            'id_jenis_kelamin' => 'required',
            'id_goldar' =>'required',
            'id_kepalakeluarga' =>'required',
            'tanggal_lahir' =>'required',
            'id_kawin' =>'required',
            'no_akta_lahir' =>'required|numeric',    
            ],
            [
    
            'nama_warga.required' => 'Field Nama  Harus Diisi!',
            'nik.required' => 'Field NIK Harus Diisi!',
            'id_pendidikan.required' => ' Harus Diisi!',
            'id_agama.required' => ' Harus Diisi!',
            'id_jenis_kelamin.required' => ' Field Jenis Kelamin Harus Diisi!',
            'id_goldar.required' => ' Field Golongan Darah Harus Diisi!',
            'id_kepalakeluarga.required' => ' Field Kepala Keluarga Harus Diisi!',
            'tanggal_lahir.required' => ' Field Tanggal Lahir Harus Diisi!',
            'id_kawin.required' => ' Status Menikah Harus Dipilih!',
            'no_akta_lahir.required' => ' No Akta Lahir Harus Diisi!',
            
            
            'nama_warga.max' => 'Maksimal 100 Karakter!',
            'nama_warga.min' => 'Minimal 3 Karakter!',
            ]
        );
    }
}

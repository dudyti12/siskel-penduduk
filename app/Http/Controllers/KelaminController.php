<?php

namespace App\Http\Controllers;

use App\Kelamin;
use Illuminate\Http\Request;

class KelaminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $kelamins = Kelamin::all();
        return view('warga.data',compact('kelamins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kelamin  $kelamin
     * @return \Illuminate\Http\Response
     */
    public function show(Kelamin $kelamin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kelamin  $kelamin
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelamin $kelamin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kelamin  $kelamin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kelamin $kelamin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kelamin  $kelamin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kelamin $kelamin)
    {
        //
    }
}

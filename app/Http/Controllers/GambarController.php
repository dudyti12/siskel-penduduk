<?php

namespace App\Http\Controllers;

use App\Gambar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GambarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $gambars = Gambar::paginate(3);
        // return view('berkas.data',compact('gambars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $this->validate($request, [
            'file' => 'required|file|image|mimes:jpeg,png,jpg,pdf,doc,docx|max:2048',
            'keterangan' => 'required',
            ]);
            
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
            
            $nama_file = time()."_".$file->getClientOriginalName();
            
                          // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);
            
            Gambar::create([
            'file' => $nama_file,
            'keterangan' => $request->keterangan,
            ]);
       return redirect('berkas')->with('status','Gambar Berhasil Disimpan!');
    // return $request;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    // public function getFile($id)
    // {
    //     $file = Gambar::find($id);

    //     // dd($file);
    //     // return Gambar::download(storage_path()."/app/public/".$file->path, $file->original_name);
    //     //also tried like this
    //     // return Storage::download(asset('storage/'.$file->path), $file->original_name);
    //     return Storage::disk('public')->get($file->path);
      

    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function show(Gambar $gambar)
    {
        //
        $gambar = Gambar::all();
        return view('berkas.data',compact('gambar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function edit(Gambar $gambar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gambar $gambar)
    {
        //
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('gambar')->where('id',$id)->delete();
        return redirect('berkas')->with('status','Gambar Berhasil Dihapus!');
        // return $id;
    }
}

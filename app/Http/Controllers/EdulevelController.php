<?php

namespace App\Http\Controllers;

use App\Edulevel;
use Illuminate\Http\Request;

class EdulevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Edulevel  $edulevel
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $edulevel = Edulevel::all();
        return view('pendidikan.dataedulevel',compact('edulevel'));
        // return $edulevel->id_edulevel;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Edulevel  $edulevel
     * @return \Illuminate\Http\Response
     */
    public function edit(Edulevel $edulevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Edulevel  $edulevel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Edulevel $edulevel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Edulevel  $edulevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Edulevel $edulevel)
    {
        //
    }
}

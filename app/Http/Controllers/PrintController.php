<?php

namespace App\Http\Controllers;

use App\Prints;
use App\Warga;
use App\Agama;
use App\Pendidikan;
use App\kelamin;
use App\Goldar;
use App\Kk;
use App\Detail;
// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
// use Barryvdh\DomPDF\PDF;

class PrintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Prints $print)
    {
        //
          $print = Prints::paginate(5);
         return view('surat.data',compact('print'));
        // dd($print);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    //     $wargas = Prints::all();
    // $pdf = PDF::loadview('warga.print')->setPaper('A4','potrait');
    // return view('warga.print',compact('wargas',$pdf->stream())); 
    // //  $pdf->stream();
    // dd($wargas);
          return view('surat.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
            DB::table('prints')->insert([
                 'nama' => $request->nama, 
                'tanggal_surat' => $request->tanggal_surat, 
                 'perihal' => $request->perihal, 
               ]);
                //  return redirect('warga')->with('status','Data Warga Berhasil Ditambah');
               return redirect('print')->with('status','Data Surat Keluar Berhasil Ditambah');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prints  $print
     * @return \Illuminate\Http\Response
     */
    public function show(Prints $prints)
    {
        //
        // $prints->makehidden(['created_at','updated_at']);
        // return $warga;
        // return view('surat.detail',compact('prints'));'
        $prints->makehidden(['created_at']);
        return $prints;
       
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prints  $print
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agama = Agama::all();
        $pendidikan = Pendidikan::all();
        $prints = Prints::all();
        $kelamin = Kelamin::all();
        $goldar = Goldar::all();
        $kk = Kk::all();
        $detail = Detail::all();
        $wargas = Warga::where('id', $id)->first();     
        return view('surat.edit',compact('wargas','agama','pendidikan','prints','kelamin','goldar','kk','detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prints  $print
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('prints')
              ->where('id', $id)
              ->update([
                  'nama' => $request->nama,
                  'tanggal_surat' => $request->tanggal_surat,
                  'perihal' => $request->perihal,
                  ]);
                  return redirect('print')->with('status','Data Surat Keluar Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prints  $print
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::table('prints')->where('id',$id)->delete();
    return redirect('print')->with('status','Data Surat Berhasil Dihapus!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Kk;
use App\Warga;
use App\Agama;
use App\Detail;
use App\Pendidikan;
use App\Goldar;
use App\Kelamin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class KkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$wargas = Warga::all();
        // return $wargas;
       
       
        $kk = Kk::paginate(5);
        return view('keluarga.data',compact('kk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kk = Kk::all();
        return view('keluarga.add',compact('kk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->_validate($request);

        $kk = new Kk();

            $kk->no_kk = $request->no_kk;
            $kk->nama_kepala_keluarga = $request->nama_kepala_keluarga;
            $kk->jumlah_anggota_klg = $request->jumlah_anggota_klg;
            $kk->rukun_tetangga = $request->rukun_tetangga;
            $kk->rukun_warga = $request->rukun_warga;
            $kk->alamat_tinggal = $request->alamat_tinggal;
            $kk->kecamatan = $request->kecamatan;
            $kk->kelurahan = $request->kelurahan;
            $kk->save();  
                     return redirect('kk')->with('status','Data Warga Berhasil Ditambah');
                    // return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kk $kk)
    {
        //
        //  $kk->makehidden(['id']);
        return view('keluarga.detail',compact('kk'));
    //    dd($kks);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Warga $warga)
    {
        //


        $kks = DB::table('kks')->where('id', $id)->first();;
       
        //dd($data_warga);
        return view('keluarga.edit',compact('kks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->_validate($request);
        $kk = Kk::find($id);
        $kk->nama_kepala_keluarga = $request->nama_kepala_keluarga;
        $kk->no_kk = $request->no_kk;
        $kk->jumlah_anggota_klg = $request->jumlah_anggota_klg;
        $kk->rukun_tetangga = $request->rukun_tetangga;
        $kk->rukun_warga = $request->rukun_warga;
        $kk->alamat_tinggal = $request->alamat_tinggal;
        $kk->kecamatan = $request->kecamatan;
        $kk->kelurahan = $request->kelurahan;
        $kk->save();
        return redirect('kk')->with('status','Data KK Berhasil di Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('kks')->where('id',$id)->delete();
        return redirect('kk')->with('status','Data KK Berhasil Dihapus!');
    }

    public function _validate(Request $request){

        $request->validate([
            'no_kk' => 'required',
            'nama_kepala_keluarga' => 'required|max:100|min:3',
            'jumlah_anggota_klg' => 'required|numeric',
            'rukun_tetangga' => 'required|numeric',
            'rukun_warga' => 'required|numeric',
            'alamat_tinggal' => 'required',
            'kecamatan' =>'required',
            'kelurahan' =>'required', 
            ],
            [
    
            'no_kk.required' => 'Field Nomor KK Harus Diisi!',
            'nama_kepala_keluarga.required' => 'Field Kepala Keluarga Harus Diisi!',
            'jumlah_anggota_klg.required' => ' Harus Diisi!',
            'rukun_tetangga.required' => ' Harus Diisi!',
            'rukun_warga.required' => ' Harus Diisi!',
            'alamat_tinggal.required' => ' Field  Harus Diisi!',
            'kecamatan.required' => ' Field  Diisi!',
            'kelurahan.required' => ' Field  Diisi!',
            
            ]
        );


    }
}

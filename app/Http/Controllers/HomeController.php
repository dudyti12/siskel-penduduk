<?php

namespace App\Http\Controllers;

use App\Warga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

      

        
        $wargas = Warga::get();
        $count = DB::table('wargas')->count();
        $laki= DB::table('wargas')->select(DB::raw('count(*) as wargas, id'))->where('id_jenis_kelamin', '=', 1)->groupBy('id')->get()->count();
        $cewek= DB::table('wargas')->select(DB::raw('count(*) as wargas, id'))->where('id_jenis_kelamin', '=', 2)->groupBy('id')->get()->count();
        $kk= DB::table('kks')->count();
        $fasum= DB::table('data_fasum')->count();
        $kantor= DB::table('data_perkantoran')->count();
        // $lansia = DB::table('wargas')->whereYear('tanggal_lahir','<=','1956')->get()->count();

        // $tahun = DB::table('tr_visit')->select(DB::raw('YEAR(tanggal_lahir)')->get();
    //    $lansia = Carbon::parse($wargas->tanggal_lahir)->diff(Carbon::now())->format('%y years, %m months and %d days');
     
    //  $tanggal= DB::table('wargas')
    //  ->whereYear('tanggal_lahir','>' ,'1988')
    //  ->get();
        // $start_Date='1950-01-01';
        // $start = Carbon::parse($start_Date);
        // $now = Carbon::now();
        // $wargas = Warga::where(['tanggal_lahir','=>' ,$start_Date ])->count();
        // $length = $start->diffInYears($now);
        // $date = Warga::select('tanggal_lahir')->find('2');
        // $tahun = Carbon::parse($date->tanggal_lahir)->year;

        // $hasil = $tahun -  diffInYear($now)

        //1.ambil tahun dari db 
       return view('home',compact('count','laki','cewek','kk','fasum','kantor','wargas'));
          
     
    }

    public function cekumur(){
        //1. tanggal lahir dari db
        $year = Warga::select('tanggal_lahir')->find(2);// mengambil id 
         //2.parse untuk ekstraksi mengambil nilai tahun only! 
        $tahun = Carbon::parse($year->tanggal_lahir)->year;
        //2.Hitung Umur dengan mengurangi tahun lahir  dengan tanggal hari ini:!
        $now = Carbon::now()->year;
        $umur =$now-$tahun;
        //3.set ambang  batas umur ,cek umur < ambang batas
        $limit = 65;
        if ($umur <= $limit) {
            echo "Bukan Orang Tua ,";
    
        } else{

            echo "Orang tua!,";
        }
        return "&nbsp;Karena Umurnya:&nbsp;".$umur."&nbsp;Tahun";
        //4.Count seluruh record dari hasil pengecekan

    }




}

<?php

namespace App\Http\Controllers;

use App\Data_fasum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;


class FasumController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('fasum.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::table('data_fasum')->insert([
            'nama_fasum' => $request->nama_fasum,
            'alamat_fasum' => $request->alamat_fasum,
            'kordinat' => $request->kordinat,
          
            ]);

            return redirect('fasum')->with('status','Data Fasilitas Umum Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Data_fasum  $data_fasum
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
      //  $data_fasum = DB::table('data_fasum')->get()->paginate(5);
        //dd($data_kk);
        $data_fasum = Data_fasum::paginate(5);
      return view('fasum.data',compact('data_fasum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     *@param  \App\Models\Data_fasum  $data_fasum
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data_fasum = DB::table('data_fasum')->where('iddata_fasum', $id)->first();;
       
        //dd($data_warga);
        return view('fasum.edit',compact('data_fasum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Data_fasum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        DB::table('data_fasum')->where('iddata_fasum',$id )
        ->update([
            'nama_fasum' => $request->nama_fasum,
            'alamat_fasum' => $request->alamat_fasum,
            'kordinat' => $request->kordinat,
          ]);
          return redirect('fasum')->with('status','Data Fasilitas Umum Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     *@param  \App\Models\Data_fasum
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('data_fasum')->where('iddata_fasum', $id)->delete();
        return redirect('fasum')->with('status','Data Fasilitas Umum Berhasil Dihapus!');

    }
}

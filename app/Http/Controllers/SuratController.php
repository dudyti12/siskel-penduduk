<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warga;
use App\Surat;
use App\Detail;
use App\Kk;
use App\Goldar;
use App\Agama;
use App\Cetak;
use App\Kelamin;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
class SuratController extends Controller
{
    //

    public function index(){

        $warga = Warga::paginate(5);
        return view('surat.data',compact('warga'));


    }

    public function create(){

        $surat = Surat::all();
        $warga = Warga::all();
        return view('surat.add',compact('surat','warga'));

    }

    public function store(Request $request){

        $warga = new Surat();
        $warga->perihal = $request->perihal;
        $warga->tanggal_surat = $request->tanggal_surat;
        $warga->save();        
        return redirect('warga')->with('status','Surat Berhasil Dibuat!');
    }

    public function search(Request $request)
    {
        
        $search = $request->search;
        $warga = Warga::where('perihal','like',"%".$search."%")
        ->Orwhere('nama_warga','like',"%".$search."%")->paginate(3);
        return view('surat.data',compact('warga')); 


    }


    public function tampil($id){
        
        $wargas = Warga::find($id);
        $surat = Surat::get();
        $kelamin = Kelamin::get();
        $goldar = Goldar::get();
        $kk = Kk::get();
        $detail = Detail::get();
        $agama = Agama::get();
        return view('surat.add',compact('wargas','surat','kelamin','kk','detail','goldar','agama'));
    }
    public function lempar(Warga $warga){

        $warga = Warga::get();
        return view('surat.test',compact('warga'));
    }

    public function updatemail(Request $request,$id){

        $warga = Warga::find($id);
        $warga->id_keperluan =  $request->id_keperluan;
        $warga->nik =  $request->nik;
        $warga->tanggal_surat =  $request->tanggal_surat;
        $warga->perihal = $request->perihal;
        $warga->save();
        return redirect('surat')->with('status','surat berhasil dibuat!silahkan Cetak');

    }

    public function proses(Warga $warga, Surat $surat, $id)
    {
        $this->tampil($id);
        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');
        \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
     
       
        $warga = Warga::find($id);
        $surat = Surat::get();
      return view('surat.cetak',compact('warga','surat'));
        // return $today;

    }

    public function show()
    {
        $surats = Surat::paginate(5);
        return view('kategori.data',compact('surats'));
    }
    public function tambah(){

        $surats = Surat::all();
        return view('kategori.add',compact('surats'));

    }
    
    public function simpan(Request $request){

        
            $surats = new Surat();
            $surats->nomor = $request->nomor;
            $surats->perihal = $request->perihal;
            $surats->save();
            return redirect('kategori')->with('status','Kategori Surat berhasil! Silahkan Buat Surat!');
    }

    public function hapus($id)
    {

        $surats = Surat::find($id);
        
        $surats->delete();
        return redirect('kategori')->with('status','Kategori Berhasil Dihapus!');

    }

    
}

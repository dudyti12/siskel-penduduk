<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Warga extends Model
{
    //
   
   
    public $timestamps = false;
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $table = 'wargas';


    public function kelamin()
    {

        return $this->belongsTo('App\Kelamin','id_jenis_kelamin');

    }

    public function pendidikan()
    {

        return $this->belongsTo('App\Pendidikan','id_pendidikan');

    }

    public function goldar(){

      return $this->belongsTo('App\Goldar','id_goldar');

    }
    public function kk(){

      return $this->belongsTo('App\Kk','id_kepalakeluarga');

    }

    public function agama(){

      return $this->belongsTo('App\Agama','id_agama');

    }

    public function surat(){

      return $this->belongsTo('App\Surat','id_keperluan');

    }
    public function getAgeAttribute()
{
    return Carbon::parse($this->attributes['tanggal_lahir'])->age;
}

public function getDates()
{
    //define the datetime table column names as below in an array, and you will get the
    //carbon objects for these fields in model objects.
    $time = [
            'tanggal_lahir'

    ];

    return array('tanggal_lahir');
}

    public function detail(){

      return $this->belongsTo('App\Detail','id_kawin');

    }
    
    //protected $primaryKey = 'iddata_warga';
    protected $fillable = [
        'id',
        'nik',
        'nama_warga',
        'tanggal_lahir',
        'jenis_kelamin',
        'agama',
        'gol_darah',
        'pendidikan',
        'pekerjaan',
        'no_akta_lahir',
        'no_akta_kawin',
        'tempat_lahir',
        'status_kawin',
        'nama_ayah',
        'nama_ibu',
 
 
 ];

 //public function Pendidikan(){
   //return $this->belongsTo('App\Models\data_warga');
  // return $this->hasOne('App\Models\Pendidikan', 'kode_pendidikan','iddata_warga');
//}
// public function pendidikans(){

//   return $this->hasMany('App\Pendidikan');
// }

   
    public function setUpdatedAt($value)
 {
   return NULL;
 }


 public function setCreatedAt($value)
 {
   return NULL;
 }

    public function user(){

       return $this->belongsTo('App\User');
   }

   public function print(){

      return $this->belongsToMany('App\Print');

   }
  

   
   
  
   
}

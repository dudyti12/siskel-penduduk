<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    //
    public function warga(){

        return $this->hasOne('App\Warga');

    }
    public function surat(){

        return $this->belongsTo('App\Surat');

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edulevel extends Model
{
    //
    public function program()
    {

        return $this->hasOne('App\Program');

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kk extends Model
{
    //
    public $timestamps = false;
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $table = 'kks';
    public function warga(){

        return $this->hasOne('App\Warga');

    }

    protected $fillable = [
        'id',
        'no_kk',
        'nama_kepala_keluarga',
        'jumlah_anggota_klg',
        'rukun_tetangga',
        'rukun_warga',
        'alamat_tinggal',
        'kecamatan',
        'kelurahan',
 ];
}
